#include <stdio.h>
#include <stdlib.h>

int main()
{
  char string[256];
  printf("DIGITE ALGO: ");
  gets(string);

  int characters = 0;
  for(int i = 0; i <=256; i++)
  {
    if(string[i] == '\0')
    {
      break;
    }

    characters++;
  }

  printf("TEM %d caracteres em '%s'\n", characters, string);
  system("pause");
}