#include <stdio.h>
#include <stdlib.h>

int numbers[20] = {0};
int last_index = -1;

void add_number()
{
    if(last_index == 19)  
    {
        printf("Vetor cheio!\n\n");
        return;
    }

    int number;
    printf("Digite um numero (0 para sair): ");
    scanf("%d", &number);

    if(number == 0)
    {
        return;
    }

    last_index++;
    numbers[last_index] = number;
}

void list_numbers()
{
    for(int i = 0; i <= last_index; i++)
    {
        printf("[%d] => %d\n", i, numbers[i]);
    }
}

void main()
{
    int choice = -1;

    while(choice != 0)
    {
        system("cls");
        printf("[1] Adicionar Numero\n[2] Listar Numeros\n[0] Sair\n\nESCOLHA: ");
        scanf("%d", &choice);
        system("cls");

        
        switch (choice)
        {
            case 1:
                add_number();
                break;
            case 2:
                list_numbers();
                break;
            case 0:
                break;
        }

        system("pause");
    }    
}