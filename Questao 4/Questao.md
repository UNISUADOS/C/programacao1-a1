4) (Nível 2 - 2,0 Pontos)

Dada uma sequência numérica do tipo:

S = 1 + 3 + 5 + ... + 99

Desenvolva um programa que faça a chamada a uma função que retorne o valor de S

Desenvolva apenas a função, mas de duas maneiras: sem recursividade e com recursividade