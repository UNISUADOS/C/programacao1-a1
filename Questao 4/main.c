int sum()
{
  int sum = 0;
  for(int i = 1; i <= 99; i += 2)
  {
    sum+=i;
  }

  return sum;
}

int sum_recursive(int number)
{
  if(number > 99)
  {
    return 0;
  }

  return number + sum_recursive(number + 2);
}

//chamar sum_recursive assim:
//int resultado = sum_recursive(1);