void sort(int numbers[20])
{
  for(int wall = 20; wall > 2; wall--)
  {
    for(int i = 0; i < wall - 1; i++)
    {
      int current = numbers[i];
      int next = numbers[i+1];

      if(current > next)
      {
        numbers[i] = next;
        numbers[i+1] = current;
      }
    }
  }
}
