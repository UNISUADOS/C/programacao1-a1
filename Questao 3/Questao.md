3) (Nível 3 - 2,0 Pontos)

Sabemos que para melhor pesquisa em um cadastro, seja ele em forma de vetor, matriz ou registro, o fato da estrutura estar ordenada, facilita muito a busca de seus elementos, principalmente se for uma estrutura muito grande. Crie um procedimento para organizar um vetor número de 20 posições, levando em consideração que o mesmo não possua elementos repetidos.