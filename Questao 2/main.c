int numbers[20] = {0};
int last_index = -1;

int greatest_number()
{
    if(last_index == -1)
    {
        return 0;
    }

    int largest = numbers[0];

    for(int i = 1; i <= last_index; i++)
    {
        int number = numbers[i];

        if(number > largest)
        {
            largest = number;
        }
    }

    return largest;
}